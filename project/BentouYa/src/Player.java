import java.io.IOException;

public class Player {

	private int money = 40000; //所持金
	private int zaiko; //弁当の在庫

	public int prepareBento(int kousui) throws IOException {

		System.out.println();

		@SuppressWarnings("resource")
		String zaiko_kari = new java.util.Scanner(System.in).nextLine();
		zaiko = Integer.parseInt(zaiko_kari);

		if (zaiko == 10) {
			return (prepareBento(kousui));
		}
		if (zaiko == 13) {
			return (prepareBento(kousui));
		} else {

			money = money - (400 * zaiko);

			return zaiko;
		}

	}
	//	void	prepareBento(int kousui)	弁当の準備をする．
	//	Playerの場合，降水確率を表示する．
	//	いくつ弁当を作るか決める(Playerの場合はキーボードから入力して貰う，CompPlayerの場合は適当な戦略で計算して決める)．
	//	作ると決めた数を zaiko に入れる．
	//	money を原価 * zaiko だけ減らす．

	public int sellBento(int tenki) throws IOException {//	弁当の在庫を売る．

		switch (tenki) {//天気 0 = 晴, 1 = 曇, 2 = 雨
		case 0:
			if (zaiko > 100) {
				zaiko = 100;
			}
			;
			money += 500 * zaiko;
			break;

		case 1:
			if (zaiko > 50) {
				zaiko = 50;
			}
			;
			money += 500 * zaiko;
			break;

		case 2:
			if (zaiko > 30) {
				zaiko = 30;
			}
			;
			money += 500 * zaiko;
			break;

		}
		return zaiko;
	}

	int getMoney() {//	所持金を返す．
		return money;

	}

	//	最大幾つ売れるかは天気次第なので，引数の tenki (0 = 晴れ，1 = 曇り, 2 = 雨)で決定する．
	//	money を売れた分だけ増やす．
	//	弁当が売れた数を返す．

}
