import java.io.IOException;

// SimpleBentoGame.java
// コンピュータの店がない簡易版弁当屋さん経営ゲーム
public class SimpleBentoGame {
    public static void main(String[] args) throws IOException {
	Player player = new Player();
	Weather w = new Weather();

	System.out.println("いきなりですが、あなたはお弁当屋さんの店長になりました。");
	System.out.println("おめでとうございます。それでは資金を100万円にしましょう");

	while (player.getMoney()<1000000 || player.getMoney()>=0) {
	    w.makeWeather();

	    System.out.println("晴れの日　：　100個売れる");//天気 0 = 晴, 1 = 曇, 2 = 雨
	    System.out.println("曇りの日　：　50個売れる");
	    System.out.println("雨の日　：　30個売れる");

	    System.out.println("あなたの資金: " + player.getMoney() + "円");

	    System.out.println("降水確率は"+w.getKousuiKakuritsu()+"%です");

	    System.out.println("何個販売するのか決めてください");

	    player.prepareBento(w.getKousuiKakuritsu());

	    System.out.println("天気は..." + w.getTenkiString() + "でした");

	    int sold = player.sellBento(w.getTenki());

	    System.out.println("あなたの店の弁当は" + sold + "個売れました");
	    System.out.println();
	}
	if(player.getMoney()<1000000) {
		 System.out.println("達成おめでとう！");
	}else if( player.getMoney()<=0) {
		System.out.println("閉店です");
	}
    }
}
